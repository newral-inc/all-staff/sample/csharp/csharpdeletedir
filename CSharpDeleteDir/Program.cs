﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CSharpDeleteDir
{
    class Program
    {
        static void Main(string[] args)
        {
            var path = "dir1";
            Console.WriteLine($"dir1 exists={Directory.Exists(path)}");

            Directory.CreateDirectory(path);
            Console.WriteLine($"dir1 exists={Directory.Exists(path)}");

            Directory.Delete(path);
            Console.WriteLine($"dir1 exists={Directory.Exists(path)}");
        }
    }
}
